#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Handy', '0.0')
from gi.repository import Gtk, Handy, Gio, GObject
from datetime import datetime, timezone
import pytz
import sys

import ptv


def init_combo_box(combobox):
    cell = Gtk.CellRendererText()
    cell.set_property('wrap-width', 200)
    cell.set_property('wrap-mode', 0)
    combobox.pack_start(cell, True)
    combobox.add_attribute(cell, "text", 1)


class Application(Gtk.Application):
    def __init__(self):
        super(Application, self).__init__()
        self.connect('activate', self.new_window)

    def new_window(self, *args):
        AppWindow(self)


class ComboBoxEntry(GObject.Object):
    def __init__(self, entry_id, name):
        super(ComboBoxEntry, self).__init__()
        self.id = entry_id
        self.name = name


class AppWindow:
    def __init__(self, application):
        self.application = application
        builder = Gtk.Builder()
        builder.add_from_file('main_window.glade')
        builder.connect_signals(Handler(builder))

        window = builder.get_object('main_window')
        window.set_application(self.application)
        window.show_all()

        mode_selection = builder.get_object('mode_selection')
        mode_store = Gio.ListStore.new(ComboBoxEntry)
        for x in ptv.get_route_types():
            mode_store.append(ComboBoxEntry(x['route_type'],
                                            x['route_type_name']))
        mode_selection.bind_name_model(mode_store, lambda e: e.name)

        builder.get_object('back_button').set_property('visible', False)
        builder.get_object('refresh_button').set_property('visible', False)

        Gtk.main()


class Handler:
    def __init__(self, builder):
        self.builder = builder
        self.window = builder.get_object('main_window')
        self.header_bar = builder.get_object('header_bar')
        self.back_button = builder.get_object('back_button')
        self.refresh_button = builder.get_object('refresh_button')
        self.stack = builder.get_object('main_stack')
        self.main_page = builder.get_object('main_page')
        self.departure_page = builder.get_object('departure_page')
        self.mode_selection = builder.get_object('mode_selection')
        self.route_selection = builder.get_object('route_selection')
        self.direction_selection = builder.get_object('direction_selection')
        self.stop_selection = builder.get_object('stop_selection')
        self.departures_list = builder.get_object('departures_list')

    def on_quit(self, *args):
        Gtk.main_quit()

    def mode_selected(self, combo, _):
        i = combo.get_selected_index()
        model = combo.get_model()
        self.mode = model[i].id

        # populate routes
        route_store = Gio.ListStore.new(ComboBoxEntry)
        if self.mode in [1, 4]:
            # sort trams by route number, and deal with '3-3a'
            for x in sorted(ptv.get_routes(self.mode),
                            key=lambda k: int(k['route_number']
                                              .split('-')[0])):
                route_store.append(ComboBoxEntry(
                    x['route_id'],
                    x['route_number'] + ' ' + x['route_name']))
        else:
            for x in sorted(ptv.get_routes(self.mode),
                            key=lambda k: k['route_name']):
                route_store.append(ComboBoxEntry(x['route_id'],
                                                 x['route_name']))
        self.route_selection.bind_name_model(route_store, lambda e: e.name)

    def route_selected(self, combo, _):
        i = combo.get_selected_index()
        model = combo.get_model()
        self.route = model[i].id

        # populate directions
        direction_store = Gio.ListStore.new(ComboBoxEntry)
        for x in ptv.get_directions(self.route):
            direction_store.append(ComboBoxEntry(x['direction_id'],
                                                 x['direction_name']))
        self.direction_selection.bind_name_model(direction_store,
                                                 lambda e: e.name)

    def direction_selected(self, combo, _):
        i = combo.get_selected_index()
        model = combo.get_model()
        self.direction = model[i].id

        # populate stops
        stop_store = Gio.ListStore.new(ComboBoxEntry)
        for x in sorted(ptv.get_stops(self.route, self.mode,
                                      self.direction),
                        key=lambda k: k['stop_sequence']):
            stop_store.append(ComboBoxEntry(x['stop_id'], x['stop_name']))
        self.stop_selection.bind_name_model(stop_store, lambda e: e.name)

    def show_departures(self, _):
        i = self.stop_selection.get_selected_index()
        model = self.stop_selection.get_model()
        self.stop = model[i].id

        # populate departures page
        for widget in self.departures_list.get_children():
            self.departures_list.remove(widget)
        departures = ptv.get_departures(self.mode, self.route,
                                        self.direction, self.stop)

        now = datetime.now(timezone.utc)
        now_str = datetime.strftime(now.astimezone(
            pytz.timezone('Australia/Melbourne')), '%H:%M:%S')
        self.header_bar.set_title(f'Last updated at {now_str}')

        for departure in departures:
            estimated = departure['estimated_departure_utc']
            arrival = datetime.strptime(estimated, '%Y-%m-%dT%H:%M:%S%z')
            delta = arrival - now
            row = Handy.ExpanderRow()
            row.set_selectable(False)
            arrival_mins = int(delta.seconds / 60)
            if arrival_mins == 0:
                row.set_title('Now')
            elif arrival_mins == 1:
                row.set_title(f'{arrival_mins} min')
            else:
                row.set_title(f'{arrival_mins} mins')
            scheduled_arrival = datetime.strptime(
                departure['scheduled_departure_utc'],
                '%Y-%m-%dT%H:%M:%S%z')
            row.set_subtitle(
                datetime.strftime(
                    scheduled_arrival.astimezone(
                        pytz.timezone('Australia/Melbourne')), '%H:%M'))
            label = Gtk.Label()
            label.set_label('Train')
            row.add(label)
            row.show()
            label.show()
            self.departures_list.insert(row, -1)

        self.stack.set_visible_child(self.departure_page)
        self.back_button.set_property('visible', True)
        self.refresh_button.set_property('visible', True)

    def go_back(self, _):
        self.stack.set_visible_child(self.main_page)
        self.back_button.set_property('visible', False)
        self.refresh_button.set_property('visible', False)
        self.header_bar.set_title('')

    def refresh(self, _):
        self.show_departures(None)


def main():
    # To make sure libhandy is actually loaded in python
    Handy.Column()

    app = Application()
    app.run(sys.argv)


if __name__ == '__main__':
    main()
