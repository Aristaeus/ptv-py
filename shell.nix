{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  python3-packages = python3.withPackages (ps: with ps; [
    pygobject3
    requests
    flake8
    pytz
  ]);
in
mkShell {
  buildInputs = [ git python3-packages libhandy gobject-introspection gtk3 gnome3.glade ];
}
