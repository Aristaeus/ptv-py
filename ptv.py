import configparser
from hashlib import sha1
import hmac
import os
import requests

config_parser = configparser.ConfigParser()
config_parser.read(os.path.expanduser('~/.config/ptv-py.ini'))
config = config_parser['DEFAULT']


def get_ptv_url(request):
    url = 'https://timetableapi.ptv.vic.gov.au'
    dev_id = int(config['user_id'])
    key = str.encode(config['api_key'])
    request += '&' if '?' in request else '?'
    raw = request + f'devid={dev_id}'
    hashed = hmac.new(key, str.encode(raw), sha1)
    signature = hashed.hexdigest()
    return url + raw + f'&signature={signature}'


def get_route_types():
    url = get_ptv_url('/v3/route_types')
    r = requests.get(url=url)
    return r.json()['route_types']


def get_routes(mode):
    url = get_ptv_url(f'/v3/routes?route_types={mode}')
    r = requests.get(url=url)
    return r.json()['routes']


def get_directions(direction):
    url = get_ptv_url(f'/v3/directions/route/{direction}')
    r = requests.get(url=url)
    return r.json()['directions']


def get_stops(route, mode, direction):
    pre_url = f'/v3/stops/route/{route}/route_type/{mode}'
    pre_url += f'?direction_id={direction}'
    url = get_ptv_url(pre_url)
    r = requests.get(url=url)
    return r.json()['stops']


def get_departures(mode, route, direction, stop):
    # url = f'/v3/departures/route_type/{mode}/stop/{stop}/route/{route}'
    pre_url = f'/v3/departures/route_type/{mode}/stop/{stop}'
    pre_url += f'/route/{route}?max_results=10'
    url = get_ptv_url(pre_url)
    r = requests.get(url=url)
    return [x for x in r.json()['departures']
            if x['estimated_departure_utc']]


def get_stop_departures(stop_id, direction, route=None):
    url = get_ptv_url(
        f'/v3/departures/route_type/1/stop/{stop_id}?max_results=10'
    )
    r = requests.get(url=url)
    departures = [x for x in r.json()['departures']
                  if x['estimated_departure_utc']]
    return departures
